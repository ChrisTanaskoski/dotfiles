#!/bin/bash
DOTDIR="${DOTFILESDIR:-${HOME}/.dotfiles}"

echo "Linking .zshenv"
ln -sf "${DOTDIR}/zdotdir/.zshenv" "${HOME}"/.zshenv

if [ -d "${DOTDIR}" ]; then
  for DOT_FILE in $( ls -A "${DOTDIR}/ln" )
    do
        ln -sf "${DOTDIR}/ln/${DOT_FILE}" "${HOME}/${DOT_FILE}" 
    done
fi

echo "Installing TPM plugins"
~/.tmux/plugins/tpm/bin/install_plugins